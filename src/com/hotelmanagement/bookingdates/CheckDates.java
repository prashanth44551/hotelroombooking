package com.hotelmanagement.bookingdates;
import java.util.Date;
import java.io.Serializable;
import java.util.Scanner;
import com.hotelmanagement.File.AdminFileRead;
import com.hotelmanagement.adminclass.AdminClass;
import java.text.SimpleDateFormat;
public class CheckDates{
	/**
	 * calculating the number of days by calculating the difference between check in and checkout dates
	 * @return 
	 */
	public static double checkInCheckOut(String roomNo,String loginName){
		//initializing the scanner
		Scanner scanner = new Scanner(System.in);
		//creating an object of type simple date format
	        SimpleDateFormat myformat = new SimpleDateFormat("dd-MM-yyyy");
	        AdminClass adminClass = new AdminClass();
	        //declaring an variable of type boolean
	        boolean flag=true;
	        double dayBetween =0;
	        while(flag) {
	        try{
	        	
	        	//entering check in and checkout date
	        	System.out.println("enter checkin date");
		        String checkin=scanner.next();
		        System.out.println("enter checkout date");
		        String checkout=scanner.next();
		        Date dateBefore  = myformat.parse(checkin);
		        Date dateafter =  myformat.parse(checkout);
	            
	            	//condition to checkout date is after check in date
	            	if(dateBefore.getTime()>dateafter.getTime()) {
	            		System.out.println("please enter dates correctly");
	            		flag=true;
	            	}
	            	else {
	            		//calculating the number of days
	            		long difference = dateafter.getTime()-dateBefore.getTime();
	            		dayBetween = (difference/(1000*60*60*24));
	            		adminClass.admin(roomNo,loginName,checkin,checkout);
	            		AdminFileRead.adminFileRead(adminClass);
	            		break;
	            	} 
	        }
	      
	        catch(Exception e)
	        {
	            System.out.println("dates should be in a String format like...dd-mm--yyyy");
	            
	        }
	        }
	        return dayBetween;
	    

	}


}
