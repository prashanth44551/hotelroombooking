package com.hotelmanagement.bookingdates;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import com.hotelmanagement.login.UserLogin;
import com.hotelmanagement.paymentdetails.AmountDisplay;
import com.hotelmanagement.register.Payment;
import com.hotelmanagement.register.Roomnum;
import com.hotelmanagement.adminclass.AdminClass;

public class Booking
{ 
	//declaring static variables to count number of rooms 
	static int numofacsingle;
	static int numofnonacsingle;
	static int numofacdouble;
	static int numofnonacdouble;
	static double totalNoOfDays;
	static double totalAcSingleDays = 0;
	static double totalAcDoubleDays = 0;
	static double totalNonAcSingleDays = 0;
	static double totalNonAcDoubleDays = 0;
	/**
	 * Booking rooms based on the user choice
	 */
	public static void booking(String loginName)
	{
		Payment pay = new Payment();
		//initializing the variables to count the number of rooms
		 numofacsingle = 0;
		 numofnonacsingle = 0;
		 numofacdouble = 0;
		 numofnonacdouble = 0;
		 
		 AmountDisplay amountDisplay = new AmountDisplay();
		 amountDisplay.set();
		 System.out.println("Cost of AC Single Room : "+amountDisplay.getAcSingleAmount());
		 System.out.println("Cost of AC Double Room : "+amountDisplay.getAcDoubleAmount());
		 System.out.println("Cost of NON-AC Single Room : "+amountDisplay.getNonAcSingleAmount());
		 System.out.println("Cost of NON-AC Double Room : "+amountDisplay.getNonAcDoubleAmount());
		 //initializing the scanner class
		Scanner scanner = new Scanner(System.in);
		//creating a list to store the room numbers
		List<String> roomnum = new ArrayList<String>();
		int book = 0;
		//repeat the loop, if selected rooms are more than the given number of rooms 
		while(book == 0) {
		System.out.println("Enter no. of rooms you want to book:");
		int noOfRooms = scanner.nextInt();
		//enter number of AC rooms...
		System.out.println("enter No. of AC rooms");
		int acRooms = scanner.nextInt();
		//enter number of NON-AC rooms...
		System.out.println("enter No. of NON-AC rooms");
		int nonacRooms = scanner.nextInt();
		int rooms = acRooms+nonacRooms;
		//If number of rooms is less than number of AC and non AC rooms then again loop repeats for selection ac or non ac room
		if(rooms>noOfRooms){
			System.out.println("given rooms is less.....than selected rooms.....");
		}else{
			int vacancy = 0;
			
			//if number of AC rooms greater than zero then enters into loop for selection of single or double bed room 
			
                    //repeat the loop until acRooms is equal to zero 				
					while(acRooms > 0)
					{
						System.out.println("select ac-rooms\n 1.Single Room\n 2.Double Rooms");
						int roomSelect = scanner.nextInt();
						switch(roomSelect)
						{
							case 1:while(vacancy == 0) {
									System.out.println("select rooms from 301-308");
									String roomNo = scanner.next();
									roomnum = Roomnum.bookingFileRead();
									
									for(int i=roomnum.size()/2;i<3*roomnum.size()/4;i++)
									{
										if(roomnum.get(i).equals(roomNo))
										{
											
											numofacsingle++;
											acRooms--;
											double numberOfDays=CheckDates.checkInCheckOut(roomNo,loginName);
											System.out.println("number of days"+numberOfDays);
											totalAcSingleDays=numberOfDays;
											System.out.println(roomNo+" is successfully booked....");
											String s=roomNo+" is booked";
											roomnum.set(i, s);
											Roomnum.roomsFileWrite(roomnum);
											vacancy = 1;
											break;
										}
									}
									if(vacancy == 0) {
										System.out.println(roomNo+"is already booked");
									}
									break;
								}
				        		break;
							case 2:while(vacancy == 0) {
									System.out.println("select rooms from 401-408");
									String roomNo = scanner.next();
									roomnum = Roomnum.bookingFileRead();
					
									for(int i=3*roomnum.size()/4;i<roomnum.size();i++)
									{
										if(roomnum.get(i).equals(roomNo))
										{
											acRooms--;
											numofacdouble++;
											double numberOfDays=CheckDates.checkInCheckOut(roomNo,loginName);
											System.out.println("number of days"+numberOfDays);
											totalAcDoubleDays=numberOfDays;
											System.out.println(roomNo+" is successfully booked....");
											String s=roomNo+" is booked";
											roomnum.set(i, s);
											Roomnum.roomsFileWrite(roomnum);
											vacancy = 1;
											break;
										}
									}
									if(vacancy == 0) {
										System.out.println(roomNo+"is already booked");
									}
									break;
								}
				
								break;
							}
							if(acRooms == 0) {
								break;
							}
						}
						
			//if number of Non-AC rooms greater than zero then enters into loop for selection of single or double bed room
					
						//repeat the loop until nonacRooms is equal to zero 	
						while(nonacRooms  > 0)
						{
							System.out.println("select nonac-rooms\n 1.Single Room\n 2.Double Rooms");
							int roomSelect = scanner.nextInt();
							switch(roomSelect)
							{
								case 1:while(vacancy == 0) {
										System.out.println("select rooms from 101-108");
										String roomNo = scanner.next();
										//reading room numbers from file and storing in to file
										roomnum = Roomnum.bookingFileRead();
						
										for(int i=0;i<roomnum.size()/4;i++)
										{
											if(roomnum.get(i).equals(roomNo))
											{
											
												numofnonacsingle++;
												nonacRooms--;
												double numberOfDays=CheckDates.checkInCheckOut(roomNo,loginName);
												System.out.println("number of days"+numberOfDays);
												totalNonAcSingleDays=numberOfDays;
												System.out.println(roomNo+" is successfully booked....");
												String s=roomNo+" is booked";
												roomnum.set(i, s);
												Roomnum.roomsFileWrite(roomnum);
												vacancy = 1;
												break;
											}
										}
										if(vacancy == 0) {
										System.out.println(roomNo+"is already booked");
										}
										break;
									}
					
									break;
								case 2:while(vacancy == 0) {
											System.out.println("select rooms from 201-208");
											String roomNo = scanner.next();
											roomnum = Roomnum.bookingFileRead();
				
											for(int i=roomnum.size()/4;i<roomnum.size()/2;i++)
											{
												if(roomnum.get(i).equals(roomNo))
												{
													nonacRooms--;
													numofnonacdouble++;
													double numberOfDays=CheckDates.checkInCheckOut(roomNo,loginName);
													System.out.println("number of days"+numberOfDays);
													totalNonAcDoubleDays=numberOfDays;
													System.out.println(roomNo+" is successfully booked....");
													String s=roomNo+" is booked";
													roomnum.set(i, s);
													Roomnum.roomsFileWrite(roomnum);
													vacancy = 1;
													break;
												}

											}
											if(vacancy == 0) {
												System.out.println(roomNo+"is already booked");
											}
											break;
										}
							}
							if(nonacRooms == 0) {
									break;
							}
									
						}
								
					break;
				}
			}
			
	}
	public static void payment()
	{
		
		Payment.billing(numofacsingle, numofacdouble, numofnonacsingle, numofnonacdouble,totalAcSingleDays,totalAcDoubleDays,totalNonAcSingleDays,totalNonAcDoubleDays);
	}
}
