package com.hotelmanagement.File;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import com.hotelmanagement.login.AdminLogin;
import com.hotelmanagement.adminclass.AdminClass;
/**
 *adminLoginRead method takes argument as adminLogin and admin as a flag
 * @author HotelManagement
 *
 */
public class AdminLoginRead 
/**
 * adminLoginRead takes object of AdminLogin type and checks the given name and password matches with the registered one 
 */
{
	public static int adminLoginRead(AdminLogin adminLogin,int admin) {
		//creating an object of type AdminLogin
		List<AdminLogin> adminLoginRead=new ArrayList<AdminLogin>();
		try {
			FileInputStream inputStream = new FileInputStream("Data4");
			ObjectInputStream objectStream = new ObjectInputStream(inputStream);
			adminLoginRead=(ArrayList)objectStream.readObject();
			for(int i=0;i<adminLoginRead.size();i++) {
				//condition to check  given name and password matches with the registered name and password
				if((adminLoginRead.get(i).getName().equals(adminLogin.getName()))&&(adminLoginRead.get(i).getPassword().equals(adminLogin.getPassword()))) {
					System.out.println("Login successful.....");
					admin = 1;
				}
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return admin;
	}
}
