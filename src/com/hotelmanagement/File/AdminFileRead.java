package com.hotelmanagement.File;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import com.hotelmanagement.register.OnlineUserRegistration;
import com.hotelmanagement.adminclass.AdminClass;

public class AdminFileRead {
	/**
	 * adminFileRead method appends the new object to the existing data
	 * @param adminClass
	 */
	public static void adminFileRead(AdminClass adminClass)
	{
		//creating an object of type AdminClass
		List<AdminClass> admin=new ArrayList<AdminClass>();
		try {
			FileInputStream inputStream = new FileInputStream("Data3");
			ObjectInputStream objectStream = null;
			if(inputStream.available()>0) {
				objectStream = new ObjectInputStream(inputStream);
				admin=(ArrayList)objectStream.readObject();
			}
			admin.add(adminClass);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		//calling adminFileWrite method
		AdminFileWrite.adminFilewrite(admin);
	}
	/**
	 * adminRead reads room number from user and checks it with the room numbers in the list if it matches details will be displayed 
	 * @param roomNum 
	 * @return 
	 */
	public static List<AdminClass> adminRead(String roomNum) {
		//creating an object of type AdminClass
		List<AdminClass> admin=new ArrayList<AdminClass>();
		
		try {
			FileInputStream inputStream = new FileInputStream("Data3");
			ObjectInputStream objectStream =  null;
			if(inputStream.available()>0) {
				objectStream = new ObjectInputStream(inputStream);
				admin=(ArrayList)objectStream.readObject();
				for(int i=0;i<admin.size();i++) {
					//condition to check given room number matches with the booked room numbers
					if(roomNum.equals(admin.get(i).getRoomNo())) {
						System.out.println(admin.get(i));
					}
				}
			}
		}catch (Exception e) {
			
			e.printStackTrace();
		}
		return admin;
	}
}
