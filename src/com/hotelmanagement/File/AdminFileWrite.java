package com.hotelmanagement.File;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import com.hotelmanagement.adminclass.AdminClass;
/**
 * stores user name and password of admin
 * @author HotelManagement
 *
 */
public class AdminFileWrite {
	/**
	 * 
	 * @param admin arrayList of type AdminClass 
	 */
	public static void adminFilewrite(List<AdminClass> admin) {
		try {
			  FileOutputStream fos = new FileOutputStream("Data3",false);
	          ObjectOutputStream oos = new ObjectOutputStream(fos);
	          //adding room numbers to the admin list
	          oos.writeObject(admin);
	          oos.close();//closing ObjectOutputStream
	          fos.close();//closing FileOutputStream 
				
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		
	}
}
