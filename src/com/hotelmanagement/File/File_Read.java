package com.hotelmanagement.File;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import com.hotelmanagement.register.OnlineUserRegistration;
/**
 *Storing data into files....
 *using FileOutputStream and ObjectOutputStream...
 *
 * @author File_Read
 *
 *@param user is a reference of ArrayList class
 *@param oos is a reference of FileOutputStream class
 *@param fos is a reference of ObjectOutputStream class
 *
 */
public class File_Read {
	public static void fileRead(List<OnlineUserRegistration> user) {
		try {
			  FileOutputStream fos = new FileOutputStream("Data1",false);
	          ObjectOutputStream oos = new ObjectOutputStream(fos);
	          oos.writeObject(user);
	          oos.close();
	          fos.close();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	
	}

}
