package com.hotelmanagement.File;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.hotelmanagement.login.AdminLogin;
/**
 * adding admin login credentials to file
 * @author HotelManagement
 *
 */
public class AdminLoginWrite {
	/**
	 * adminLoginWrite method writes admin user name ands password to file
	 */
	public static void adminLoginWrite() {
		//creating an object of type AdminLogin
		AdminLogin adminLogin = new AdminLogin();
		try {
			  FileOutputStream fos = new FileOutputStream("Data4");
	          ObjectOutputStream oos = new ObjectOutputStream(fos);
	          //creating an array list of type AdminLogin
	          List<AdminLogin> adminLoginwrite = new ArrayList<AdminLogin>();
	          adminLogin.setDetails();
	          adminLoginwrite.add(adminLogin);
	          oos.writeObject(adminLoginwrite);
	          oos.close();//closing ObjectOutputStream
	          fos.close();//closing FileOutputStream
				
			} catch (Exception e) {
				
				e.printStackTrace();
			}
	}
}
