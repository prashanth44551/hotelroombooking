package com.hotelmanagement.File;


import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.io.ObjectInputStream;

import java.util.ArrayList;
import java.util.List;

import com.hotelmanagement.register.OnlineUserRegistration;
/**
 * Fetching data from file and adding the data to list..
 * Store list data into file...
 * @author File_Write
 *@param Data1 is a file..
 *@param fis is a reference of FileInputStream class...
 *@param ois is a reference of ObjectInputStream class....
 *@param user is a reference of ArrayList...
 *@param userRegistration is a reference of OnlineUserRegistration class....
 */
public class File_Write {
	public static void fileRead(OnlineUserRegistration userRegistration) {
		
		try {
			List<OnlineUserRegistration> user=new ArrayList<OnlineUserRegistration>();
			File file = new File("Data1");
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = null;
			if(fis.available()>0) {
				ois = new ObjectInputStream(fis);
				user=(ArrayList)ois.readObject();
				
			}
			
			
			user.add(userRegistration);
	
			File_Read.fileRead(user);

			
			
		} catch (ClassNotFoundException e) {
		
			e.printStackTrace();
		} catch (IOException e) {
		
			e.printStackTrace();
		}
	}
	/**
	 * writing data into file
	 * @param name 
	 * @param password
	 * @param flag
	 * @return
	 */
	public static int fileRead(String name,String password,int flag) {
		try {
			File file = new File("Data1");
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);
			List<OnlineUserRegistration> user=new ArrayList<OnlineUserRegistration>();
			user=(ArrayList)ois.readObject();
			for(int i=0;i<user.size();i++) {
				if(((user.get(i).getName()).equals(name)) &&((user.get(i).getPassword()).equals(password))) {
					System.out.println("login successful....");
					flag=1;
				
				}
			}
			if(flag == 0){
				System.out.println("login Unsuccessful.....");
			}
				ois.close();//closing object stream
				fis.close();//closing file stream
				

		} catch (ClassNotFoundException e) {
		
			e.printStackTrace();
		} catch (IOException e) {
		
			e.printStackTrace();
		}
		return flag;
	}
	/**
	 * reading the data from file
	 * @param mail
	 * @param flag2
	 * @return
	 */
	public static int fileRead(String mail,int flag2) {
		try {
			File file = new File("Data1");
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);
			List<OnlineUserRegistration> user=new ArrayList<OnlineUserRegistration>();
			user=(ArrayList)ois.readObject();
			for(int i=0;i<user.size();i++) {
				if(((user.get(i).getEmailId()).equals(mail))) {
					System.out.println(user.get(i).getPassword());
					flag2=1;
				
				}
			}
				ois.close();//closing object stream
				fis.close();//closing file stream

		} catch (ClassNotFoundException e) {
		
			e.printStackTrace();
		} catch (IOException e) {
		
			e.printStackTrace();
		}
		return flag2;
	}

}
