package com.hotelmanagement.paymentdetails;
/**
 * setters and getters methods for cost of the rooms 
 * @author AmountDisplay
 *
 */
public class AmountDisplay 
{
	//declaring constant variables...
	 private double acSingleAmount;
	 private double acDoubleAmount;
	 private double nonAcSingleAmount;
	 private double nonAcDoubleAmount;

	 public AmountDisplay()
	 {
		 
	 }
	/**
	 * getter method to get the cost of AC single room amount
	 * @return acSingleAmount
	 */
	public double getAcSingleAmount() {
		return acSingleAmount;
	}
	
	/**
	 * setter method to set the cost of AC single room amount
	 * @param acSingleAmount
	 */
	public void setAcSingleAmount(double acSingleAmount) {
		this.acSingleAmount = acSingleAmount;
	}
	/**
	 * getter method to get the cost of AC double room amount
	 * @return acDoubleAmount
	 */
	public double getAcDoubleAmount() {
		return acDoubleAmount;
	}
	/**
	 * setter method to set the cost of AC double room amount
	 * @param acSingleAmount
	 */
	public void setAcDoubleAmount(double acDoubleAmount) {
		this.acDoubleAmount = acDoubleAmount;
	}
	/**
	 * getter method to get the cost of Non-AC single room amount
	 * @return nonAcSingleAmount
	 */
	public double getNonAcSingleAmount() {
		return nonAcSingleAmount;
	}
	/**
	 * setter method to set the cost of Non-AC single room amount
	 * @param nonAcSingleAmount
	 */
	public void setNonAcSingleAmount(double nonAcSingleAmount) {
		this.nonAcSingleAmount = nonAcSingleAmount;
	}
	/**
	 * getter method to get the cost of Non-AC double room amount
	 * @return nonAcDoubleleAmount
	 */
	public double getNonAcDoubleAmount() {
		return nonAcDoubleAmount;
	}
	/**
	 * setter method to set the cost of Non-AC double room amount
	 * @param nonAcDoubleAmount
	 */
	public void setNonAcDoubleAmount(double nonAcDoubleAmount) {
		this.nonAcDoubleAmount = nonAcDoubleAmount;
	}
	/**
	 * set method for setting the cost of rooms 
	 */
	public void set()
	{
		setAcDoubleAmount(2000);
		setNonAcSingleAmount(800);
		setNonAcDoubleAmount(1500);
		setAcSingleAmount(1200);
	}
	@Override
	public String toString() {
		return "AmountDisplay [acSingleAmount=" + acSingleAmount + ", acDoubleAmount=" + acDoubleAmount
				+ ", nonAcSingleAmount=" + nonAcSingleAmount + ", nonAcDoubleAmount=" + nonAcDoubleAmount + "]";
	}
	
}
