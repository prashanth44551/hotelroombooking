package com.hotelmanagement;

import java.util.Scanner;

import com.hotelmanagement.register.OnlineUserRegistration;

public class RegistrtionValidation 
{
	
	public static void registration(int flag,OnlineUserRegistration userRegistration) {
		//initializing the scanner
		Scanner scanner = new Scanner(System.in);
	
		while(flag == 0){
		
			System.out.println("enter name");
			String name = "";
			try {
				name = scanner.next();
			}catch(Exception e) {
				System.out.println("name should be in a string format...");
			}
			userRegistration.setName(name);
			if(userRegistration.getName()!=null){
				flag = 1;
			}
		}
		flag = 0;
		//entering password with validations....
		while(flag == 0){
			System.out.println("enter password");
			String password = "";
			try {
				password = scanner.next();
			}catch(Exception e) {
				System.out.println("password should be in a required format...");
			}
			userRegistration.setPassword(password);
			if(userRegistration.getPassword() != null){
				flag = 1;
			}
		}
		flag = 0;
		//enter confirm password and checks it matches or not...
		while(flag == 0){
			System.out.println("enter confirm password");
			String confirmPassword = "";
			try {
				confirmPassword = scanner.next();
			}catch(Exception e) {
				System.out.println("confirmPassword should be in a required format...");
			}
			userRegistration.setConfirmPassword(confirmPassword);
			if(userRegistration.getConfirmPassword() != null){
				flag = 1;
			}
		}
		flag = 0;
		//enter mobile number only digits...
		while(flag == 0){
			System.out.println("enter mobile number");
			String mobileNumber = "";
			try {
				mobileNumber = scanner.next();
			}catch(Exception e) {
				System.out.println("mobilenumber should be in a required format...");
			}
			userRegistration.setMobileNumber(mobileNumber);
			if(userRegistration.getMobileNumber() != null){
				flag = 1;
			}
		}
		//enter address....
		System.out.println("enter address");
		String address = "";
		try {
			address = scanner.next();
		}catch(Exception e) {
			System.out.println("address should be in a required format...");
		}
		userRegistration.setAddress(address);
		flag = 0;
		//enter email id with validations.....
		while(flag == 0){
			System.out.println("enter emailId");
			String emailId="";
			try {
				emailId = scanner.next();
			}catch(Exception e) {
				System.out.println("enter valid emailId...");
			}
			userRegistration.setEmailId(emailId);
			if(userRegistration.getEmailId() != null){
				flag = 1;
			}
		}
		
	}
}
