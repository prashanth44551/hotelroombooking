package com.hotelmanagement.register;



import java.util.Scanner;

import com.hotelmanagement.paymentdetails.AmountDisplay;


public class Payment {
	
	/**
	 * generating bill to be paid by user based on number of rooms selected.....
	 * @param numofacsingle
	 * @param numofacdouble
	 * @param numofnonacsingle
	 * @param numofnonacdouble
	 * @param totalNoOfDays 
	 */
	
	public static void billing(int numofacsingle,int numofacdouble,int numofnonacsingle,int numofnonacdouble, double totalAcSingleDays, double totalAcDoubleDays, double totalNonAcSingleDays, double totalNonAcDoubleDays)
	{
		double amount = 0;
		double amount1 = 0;
		double amount2 = 0;
		double amount3 = 0;
		AmountDisplay amountDisplay = new AmountDisplay();
		amountDisplay.set();
		//initializing the scanner class
		Scanner sc = new Scanner(System.in);
		//calculating the amount for AC single bed room based on number of days
		for(int i=0; i < numofacsingle;i++)
		{
			amount = amount+amountDisplay.getAcSingleAmount();
			
		}
		amount = amount*totalAcSingleDays;
		//calculating the amount for non-AC single bed room based on number of days
		for(int i=0; i < numofnonacsingle;i++)
		{
			amount1 = amount1+amountDisplay.getNonAcSingleAmount();
			
		}
		amount1 = amount1*totalNonAcSingleDays;
		//calculating the amount for non-AC double bed room based on number of days
		for(int i=0; i < numofnonacdouble;i++)
		{
			amount2 = amount2+amountDisplay.getNonAcDoubleAmount();
			
		}
		amount2 = amount2*totalNonAcDoubleDays;
		//calculating the amount for AC double bed room based on number of days
		for(int i=0; i < numofacdouble;i++)
		{
			amount3 = amount3+amountDisplay.getAcDoubleAmount();
			
		}
		amount3 = amount3*totalAcDoubleDays;
		//displaying total amount
		System.out.println("your payment is:"+(amount+amount1+amount2+amount3));
		int flag=1;
		while(flag == 1){
			System.out.println("enter the amount to be paid:");
			//entering amount by user
			double cash=sc.nextDouble();
			//condition to check given cash is less than than to bill generated
			if (cash < (amount+amount1+amount2+amount3)){
				System.out.println("Insufficient Balance...");
			}
			//condition to check given cash is greater than to bill generated
			else if(cash > (amount+amount1+amount2+amount3))
			{
					System.out.println("Payment is successful...");
					System.out.println("your remaining balance "+(cash-amount)+"is added to ur wallet");
					flag = 0;
			}
			//condition to check given cash is equal to bill generated
			else if(cash == (amount+amount1+amount2+amount3))
			{
					System.out.println("Payment is successful....");
					flag = 0;
			}
			else
			{
				System.out.println("enter a valid amount.....");
			}
		
		}
		
		
	}
	
}
		
		
	


