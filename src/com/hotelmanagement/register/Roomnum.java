package com.hotelmanagement.register;

import java.io.FileInputStream;

import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class Roomnum {
	//creating an array list of type string
	static List<String> roomnum = new ArrayList<String>();

	
		/**
		 * writes the updated data into file
		 */
	public static void roomsFileWrite(List<String> roomnum) {
	
		try {
			//creating an object of type FileOutputStream 
			  FileOutputStream fos = new FileOutputStream("Data2",false);
			  //creating an object of type ObjectOutputStream 
	          ObjectOutputStream oos = new ObjectOutputStream(fos);
	          oos.writeObject(roomnum);
	          oos.close();//closing ObjectOutputStream
	          fos.close();//closing the FileOuputStream
	         	
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			
	
	}
	/**
	 * roomsFile reads the room numbers and displays it to the user
	 * @return 
	 */
	
	public static List<String> roomsFileRead() {
		try {
			  FileInputStream fis = new FileInputStream("Data2");
	          ObjectInputStream ois = new ObjectInputStream(fis);
	          roomnum=(ArrayList<String>)ois.readObject();
	          
	        
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			return roomnum;
	}
	/**
	 * bookingFileRead method reads the room number if the room number is available in the vacancy list then the room is booked and removed from the vacancy list 
	 * @param roomNo
	 * @return 
	 */
	public static List<String> bookingFileRead()
	{
		
		try {
			  FileInputStream fis = new FileInputStream("Data2");
			  
	          ObjectInputStream ois = new ObjectInputStream(fis);
	          
	          roomnum=(ArrayList<String>)ois.readObject();
	          
	          roomsFileWrite(roomnum);
	          
	         
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			return roomnum;
	}
	/**
	 * add rooms method writes the data into file only if the data2 file is empty 
	 */
	public static void addRooms() {
		try {
			FileInputStream inputStream = new FileInputStream("Data2");
			//if input stream file is empty then writes the updated data into file 
			if(inputStream.available()>0) {
			ObjectInputStream ois =new ObjectInputStream(inputStream);
			roomnum = (ArrayList<String>)ois.readObject();
			ois.close();
			}
			else {
				if(roomnum.size() == 0) {
					roomnum.add("NON-AC SINGLE ROOMS");
					for(int i=101;i<=108;i++){
						roomnum.add(""+i);
					}
					roomnum.add("NON-AC DOUBLE ROOMS");
					for(int i=201;i<=208;i++){
						roomnum.add(""+i);
					}
					roomnum.add("AC SINGLE ROOMS");
					for(int i=301;i<=308;i++){
						roomnum.add(""+i);
					}
					roomnum.add("AC DOUBLE ROOMS");
					for(int i=401;i<=408;i++){
						roomnum.add(""+i);
					}		
				}
				
		}
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		roomsFileWrite(roomnum);		
	}

}
  