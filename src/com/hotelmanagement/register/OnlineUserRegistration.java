package com.hotelmanagement.register;

import java.io.Serializable;
/**
 * @param declaring variables and accessing them by using setter and getter methods...
 * 
 */
import java.util.regex.Pattern;
public class OnlineUserRegistration implements Serializable
{
	private String name;
	private String password;
	private String confirmPassword;
	private String mobileNumber;
	private String address;
	private String emailId;
	//Constructor without arguments.....
	/**
	 * The getName() method returns the name 
	 * @return
	 */
	public String getName() 
	{
		return name;
	}
	/**
	 * The setAddress() method  set the address
	 */
	public void setAddress(String address)
	{
		this.address = address;
	}
	public String getAddress() 
	{
		return address;
	}
	/**
	 * The setEmailId method set the mailId based on the validateemailId() method
	 * @param emailId
	 */
	public void setEmailId(String emailId)
	{
		//condition to validate mail id
		if(validateEmailId(emailId))
		{
			this.emailId = emailId;
		}
		else
		{
			System.out.println("please enter valid mail id....");
		}
		
		
	}
	/**
	 * The getEmailId() method returns emailId
	 * @return
	 */
	public String getEmailId() 
	{
		return emailId;
	}
	/**
	 * The setName method set the name based on the validateLength() method
	 * @param name
	 */
	public void setName(String name) 
	{
		
		if(validateLength(name,6,12))
		{
			
			this.name=name;
		}
		else
		{
			System.out.println("name is invalid....name should be min 6 chars and max 12 chars...");
		}
		
	}
	/**
	 * The getPassword method returns the password
	 * @return
	 */
	public String getPassword() 
	{
		return password;
	}
	/**
	 * The setter method to set password
	 * @param password
	 */
	public void setPassword(String password) 
	{
		if(validateLength(password,8,10)&&validateUppercaseAlpha(password)&&validateLowercaseAlpha(password)&&validateDigit(password)&&validateSpecialCharacter(password))
		{
			this.password=password;
		}
		else
		{
			System.out.println("password is invalid.....password should consist of 1 uppercase,1 lowercase,1 digit and 8-10 chars....");
		}
	}
	/**
	 * The getConfirmPassword method returns the password
	 * @return 
	 */
	public String getConfirmPassword() 
	{
		return confirmPassword;
	}
	/**
	 * The setconfirmPassword method sets the password based on the validatepassword method  
	 * @param confirmPassword
	 */
	public void setConfirmPassword(String confirmPassword) 
	{
		if(validatePassword(password,confirmPassword))
		{
			this.confirmPassword = confirmPassword;
		}
		else
		{
			System.out.println("password and confirm password doesn't matches....plz re-enter.....");
		}
	}
	/**
	 * The getMobilenumber returns the mobile number
	 * @return mobile number
	 */
	public String getMobileNumber() 
	{
		return mobileNumber;
	}
	/**
	 * The setMobileNumber sets the mobile number based on the validatelength and validatedigit
	 * @param mobileNumber
	 */
	public void setMobileNumber(String mobileNumber) 
	{
		if(validateLength(mobileNumber,10,10) && validateDigit1(mobileNumber))
		{
			this.mobileNumber=mobileNumber;
		}
		else
		{
			System.out.println("mobile number should consists of only 10 digits....");
		}
	}
	/**
	 * The validatePassword method checks the password and confirm password is correct or not....
	 * @param confirmPassword2 
	 * @param password2
	 * @return boolean value
	 */
	public boolean validatePassword(String confirmPassword2, String password2) 
	{
		boolean status = false;
		//condition to check password and confirm password match  
		if(password2.equals(confirmPassword2))
		{
			status = true;
			
		}
		else
		{
			return false;
		}
		return status;
	}
	/**
	 * ValidateEmailId method validates the mail id based on condition
	 * @param emailId
	 * @return boolean value
	 */
	public static boolean validateEmailId(String emailId)
	{
		boolean status = false;
		String email = "^[a-zA-Z0-9_+*&-]+(?:\\."+"[a-zA-Z0-9_+*&-]+)*@"+"(?:[a-zA-Z0-9-]+\\.)+[a-z"+"A-Z]{2,7}$";
		Pattern pat = Pattern.compile(email);
		if(pat.matcher(emailId).matches())
		{
			
			status = true;
		
		}
		return status;
	}
	/**
	 * validateSpecial character method returns the boolean value based on condition
	 * @param password2
	 * @return boolean value
	 */
	public static boolean validateSpecialCharacter(String password2) 
	{
		boolean status=false;
		for(int i=0;i<password2.length();i++)
		{
			char ch=password2.charAt(i);
			//condition for checking the special case character
			if(ch=='$'||ch=='@'||ch=='_'||ch=='%'||ch=='!'||ch=='^'||ch=='?'||ch=='*'||ch=='&'||ch=='('||ch==')'||ch=='<'||ch=='>'||ch=='{'||ch=='}'||ch=='/'||ch=='\\'||ch==';')
			{
				status=true;
				break;
			}
		}
		return status;
	}
	/**
	 * The validateDigit method checks for the digit in password if it occurs returns true else returns false 
	 * @param password2
	 * @return boolean value
	 */
	public static boolean validateDigit(String password2) 
	{
		boolean status=false;
		for(int i=0;i<password2.length();i++)
		{
			char ch=password2.charAt(i);
			//condition for checking the digit 
			if(Character.isDigit(ch))
			{
				status=true;
				break;
			}
		}
		return status;
	}
	/**
	 * The validatelowercaseAlpha method checks for the lower case character if it occurs it returns true else returns false 
	 * @param password2
	 * @return boolean value
	 */
	public static boolean validateLowercaseAlpha(String password2) 
	{
		boolean status=false;
		for(int i=0;i<password2.length();i++)
		{
			char ch=password2.charAt(i);
			//condition for checking the lower case character
			if(Character.isLowerCase(ch))
			{
				status=true;
				break;
			}
		}
		return status;
	}
	/**
	 * The validateUppercaseAlpha method checks for the Upper case character if it occurs it returns true else returns false
	 * @param password2
	 * @return boolean value
	 */
	public static boolean validateUppercaseAlpha(String password2) 
	{
		boolean status=false;
		for(int i=0;i<password2.length();i++)
		{
			char ch=password2.charAt(i);
			//condition to check upper case character
			if(Character.isUpperCase(ch))
			{
				status=true;
				break;
			}
		}
		return status;
	}
	/**
	 * The validateLength method validates password by checking the length of password greater than  min and less than max if it satisfies the condition it returns true else returns false  
	 * @param password2
	 * @param min
	 * @param max
	 * @return boolean value
	 */
	public static boolean validateLength(String password2, int min, int max) 
	{
		//condition for checking the length 
		if(password2.length()>=min && password2.length()<=max)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	/**
	 * The validateDigit1 method returns true if there is only digits if any character occurred it returns false 
	 * @param number
	 * @return boolean value
	 */
	public static boolean validateDigit1(String number)
	{
		boolean status=false;
		for(int i=0;i<number.length();i++)
		{
			char ch=number.charAt(i);
			//condition to check digit
			if(Character.isDigit(ch))
			{
				status=true;
			}
			else
			{
				status=false;
				break;
			}
		}
		return status;
	}
	/**
	 * overriding the toString method to print name,password,confirmPassword and mobile number
	 */
	@Override
	public String toString() {
		return "UserRegistration [name=" + name + ", password=" + password + ", confirmPassword=" + confirmPassword
				+ ", mobileNumber=" + mobileNumber + "]";
	}
	
}
