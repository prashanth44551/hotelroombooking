package com.hotelmanagement.login;

import java.io.Serializable;
import java.util.Scanner;

public class AdminLogin implements Serializable{
	//declaring the variables
	private String name;
	private String password;
	public AdminLogin(){
		
	}
	//getter method to get name 
	public String getName() {
		return name;
	}
	//setter method to set name 
	public void setName(String name) {
		this.name = name;
	}
	//getter method to get password
	public String getPassword() {
		return password;
	}
	//setter method to set password
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "AdminLogin [name=" + name + ", password=" + password + "]";
	}	
	/**
	 * method to set name and password
	 */
	public void setDetails() {
		setName("madhavi");
		setPassword("Password@1");
	}
}
