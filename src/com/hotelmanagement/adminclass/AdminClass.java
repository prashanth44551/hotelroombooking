package com.hotelmanagement.adminclass;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.hotelmanagement.register.OnlineUserRegistration;

public class AdminClass implements Serializable {
	//declaring the variables
	private String roomNo;
	private String name;
	private String mobileNumber;
	private String checkIn;
	private String checkOut;
	public AdminClass(){
		
	}
	//getter method to return checkIn date
	public String getCheckIn() {
		return checkIn;
	}
	//setter method to set checkIn date
	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}
	//getter method to return checkOut date
	public String getCheckOut() {
		return checkOut;
	}
	//setter method to set checkout date
	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}
	@Override
	public String toString() {
		return "AdminClass [roomNo=" + roomNo + ", name=" + name + ", mobileNumber=" + mobileNumber + ", checkIn="
				+ checkIn + ", checkOut=" + checkOut + "]";
	}
	//getter method to get room number
	public String getRoomNo() {
		return roomNo;
	}
	//setter method to set room number
	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}
	//getter method to return name
	public String getName() {
		return name;
	}
	//setter method to set name
	public void setName(String name) {
		this.name = name;
	}
	//setter method to return mobile number 
	public String getMobileNumber() {
		return mobileNumber;
	}
	//setter method to set mobile number
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	/**
	 * 
	 * @param roomNo
	 * @param loginName
	 * @param checkIn
	 * @param checkOut
	 */
	public void admin(String roomNo,String loginName,String checkIn,String checkOut) {
		List<OnlineUserRegistration> user=new ArrayList<OnlineUserRegistration>();
		try {
			File file = new File("Data1");
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = null;
			if(fis.available()>0) {
				ois = new ObjectInputStream(fis);
				user=(ArrayList)ois.readObject();
			}
		} catch (ClassNotFoundException e) {
				
				e.printStackTrace();
		} catch (IOException e) {
				
				e.printStackTrace();
		}	
		for(int i=0;i<user.size();i++) {
			if(loginName.equals(user.get(i).getName())) {
				setName(user.get(i).getName());
				setMobileNumber(user.get(i).getMobileNumber());
			}
		}
		setRoomNo(roomNo);
		setCheckIn(checkIn);
		setCheckOut(checkOut);
		
	}
}
