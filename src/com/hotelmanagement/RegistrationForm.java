package com.hotelmanagement;


/**
 * predefined packages
 */
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Scanner;

import com.hotelmanagement.bookingdates.Booking;
import com.hotelmanagement.File.AdminFileRead;
import com.hotelmanagement.File.AdminFileWrite;
import com.hotelmanagement.File.AdminLoginRead;
import com.hotelmanagement.File.AdminLoginWrite;
import com.hotelmanagement.File.File_Write;
import com.hotelmanagement.login.AdminLogin;
import com.hotelmanagement.login.UserLogin;
import com.hotelmanagement.register.OnlineUserRegistration;

import com.hotelmanagement.register.Roomnum;
import com.hotelmanagement.adminclass.AdminClass;
import com.hotelmanagement.admindates.AdminCheckInCheckOut;



/**
 * 
 * @author HotelManagement
 * enter the register credentials 
 * login
 * booking
 *
 */

public class RegistrationForm implements Serializable{
	/**
	 * @param flag for login
	 * @param flag1 for repeating loop if login credentials is wrong....
	 * @param flag2 for checking email id exist or  not
	 * @author main method
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		//initializing the scanner
		Scanner scanner = new Scanner(System.in);
		int flag = 0;
		while(true) {
		System.out.println("Choice option to register\n1.Online User\n 2.Admin");
		String choice = scanner.next();
		
		int userdata =0;
		//case 1:online user case 2: exit...
		switch(choice){
			case "1":while(userdata == 0) {
						System.out.println("enter number to perform operation:");
						System.out.println("1.Register\n2.Login\n3.Exit ");
						String num = scanner.next();
						
						//case 1:user Registration case 2:Login....
						switch(num){
							case "1":
								//creating object for online Registration
								OnlineUserRegistration userRegistration = new OnlineUserRegistration();
								//entering name with conditions...
								RegistrtionValidation.registration(flag,userRegistration);
								flag=0;
								System.out.println("Registered successful....");
								File_Write.fileRead(userRegistration);
								break;
							case "2"://object creation for login page...
									UserLogin login = new UserLogin();
									int flag1 = 0;
									flag = 0;
									//countRelogin is to count the number of attempts be done.....
									int countRelogin = 0;
									while(flag1 == 0){
										System.out.println("enter name");
										String name = "";
										try {
											name = scanner.next();
										}catch(Exception e) {
											System.out.println("name should be in a string format...");
										}
										//set name by using setter method....
										login.setName(name);
										System.out.println("enter password");
										String password = "";
										try {
											password = scanner.next();
										}catch(Exception e) {
											System.out.println("password should be in a required format...");
										}
										//set password by using setter method....
										login.setPassword(password);
										//checking entered name and password and registered name and password is correct or not.....
										flag=File_Write.fileRead(name, password,flag);
										flag1 = flag;
										if(flag == 0)
										{
											System.out.println("user name or password is invalid......");
											System.out.println("1. Forget Password\n 2.Re-Login");
											String choice1 = scanner.next();
											
											if(choice1.equals("2"))
												countRelogin++;
											switch(choice1)
											{
												case "1":flag1 = 1;
														int flag2 = 0;
														while(flag2 == 0)
														{
															System.out.println("enter registered EmailId:");
															String mail = scanner.next();
															//check whether entered emailId and registered emailId is correct or not....
															flag2 = File_Write.fileRead(mail,flag2);
															if(flag2 == 0)
															{
																System.out.println("entered Registered email id is invalid.....");				
													
															}
														}
														break;
												case "2":if(countRelogin <= 3)
														{
															flag1 = 0;
														}
														else
														{
															flag1 = 1;
															flag=1;
															System.out.println("login failed...Register again.....");
														}
														break;
												
												default:System.out.println("invalid input....");
											}
											
								
										}
										else
										{
											int logOut = 0;
											//declaring variable to know the count of number of rooms booked... 
											
											while(logOut == 0) {
											System.out.println("enter your choice");
											System.out.println("1.Number of vacancies\n 2.Booking\n 3.Payment\n 4.Logout");	
											String choice1 = scanner.next();
											
											
											//case 1:number of vacancies..case 2:Booking....case 3:Payment process...case 4:Logout....
											
											switch(choice1)
											{
												case "1":
													System.out.println("Number of vacancies are:");
													Roomnum.addRooms();
													List<String> roomnum = Roomnum.roomsFileRead();
													for(int i=0;i<roomnum.size();i++)
											        {
											        	  System.out.println(roomnum.get(i));
											        }
													break;
												case "2":Booking.booking(login.getName());
														break;
												case "3"://calling payment method
														Booking.payment();
														break;
												case "4":logOut=1;
														break;
											}
										}
									}
								}
								break;
							case "3":System.out.println("exited.....");
									userdata =1;
									break;
							default:System.out.println("enter valid option....");
							}
						}
						break;
						case "2":AdminLoginWrite.adminLoginWrite();
								int admin = 0;
								while(admin==0) {
									System.out.println("enter name:");
									String name = scanner.next();
									System.out.println("enter password:");
									String password = scanner.next();
									AdminLogin adminLogin = new AdminLogin();
									adminLogin.setName(name);
									adminLogin.setPassword(password);
									admin = AdminLoginRead.adminLoginRead(adminLogin,admin);
								}
								System.out.println("enter Registered Room Number to allocate:");
								String roomNum = scanner.next();
								List<AdminClass> adminData = AdminFileRead.adminRead(roomNum);
								AdminCheckInCheckOut.adminCheckIn(adminData,roomNum);
								break;
				}
		}
	}
}
		
									